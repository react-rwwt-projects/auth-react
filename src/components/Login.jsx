import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { validateUser } from '../redux/loginReducer';

const Login = () => {
  const auth = useSelector( state => state.user.auth)

  const [email,setEmail]= useState('');
  const [password,setPassword]= useState('');
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const loginHander = (e) =>{
    e.preventDefault();
    dispatch(validateUser({email,password}));
  }
  useEffect( ()=>{
    if(auth) navigate('/home')
  },[])
  return (
    <div>
      <form onSubmit={loginHander}>
        <div>
          <label htmlFor="email">Email</label>
          <input type="email" placeholder='email' id='email' onChange={(e)=>setEmail(e.target.value)} />
        </div>
        <div>
          <label htmlFor="">Password</label>
          <input type="password" placeholder='password' id="password" onChange={(e)=>setPassword(e.target.value)} />
        </div>
        <div>
          <input type="submit" value="Login" />
        </div>
      </form>
  
    </div>
  )
}

export default Login