import React from 'react'

const NotFound = () => {
  return (
    <div>
        You Got Lost!
        What are you doing here?
    </div>
  )
}

export default NotFound