import React from 'react'
import { useSelector } from 'react-redux'
import { Outlet } from 'react-router-dom'
import Login from './Login'


const Protected = () => {
    const name = useSelector( state => state.user)
    const isAuth = name.auth;
    return isAuth ? <Outlet /> : <Login />;
}

export default Protected