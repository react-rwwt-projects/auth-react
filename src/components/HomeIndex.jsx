import React, { useEffect } from 'react'
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
const HomeIndex = () => {
    const auth = useSelector(state=>state.user.auth)
    const navigate = useNavigate();
    useEffect( ()=>{
        if(auth) navigate('/home')
        if(!auth) navigate('/login')
    },[])
  return (
    null
    )
}

export default HomeIndex