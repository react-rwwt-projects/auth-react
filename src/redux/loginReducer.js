import {createAsyncThunk ,createSlice } from '@reduxjs/toolkit'
import axios from 'axios';

export const validateUser = createAsyncThunk('auth/verify', async(data)=>{ 
    const res = await axios.post('http://localhost:8080/api/user/login',
        data
    )
    return await res.data;
});    

export const verifyUser  = createSlice({
    name:"user",
    initialState:{
        pending: true,
        error: true,
        user:{
            email:'',
            password:'',
            role:''
        },
        auth: false
    },
    reducers: {
        logout:(state)=>{
            state.auth = false;
        }
    },
    extraReducers:{
        [validateUser.pending]: (state)=>{
            state.pending = true;
            state.error = false;
        },
        [validateUser.fulfilled]: (state, action)=>{
            state.pending = false;
            state.user = action.payload;
            localStorage.setItem('token',action.payload.token);
            state.auth = true;
        },
        [validateUser.rejected]: (state)=>{
            state.pending = null;
            state.error = true;
        }
    }
})
export const { logout } = verifyUser.actions; 
export default verifyUser.reducer;
  