import './App.css';
import Home from './components/Home';
import { Routes , Route } from 'react-router-dom';
import Login from './components/Login';
import Protected from './components/ProtectedRoute';
import NotFound from './components/NotFound';
import NavBar from './components/NavBar';
import { useSelector } from 'react-redux';
import HomeIndex from './components/HomeIndex';
function App() {
  const user = useSelector(state => state.user)
  return (
    <div className="App">
      {user.auth && ( <NavBar /> )}
      <Routes>
        <Route index element={<HomeIndex />} />
        <Route element={<Protected />}>
          <Route path="/websites" element={<Home />}/>
          <Route path="/home" element={<Home />}/>
          <Route path="/login" element={<Login />}/>
        </Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </div>
  );
}

export default App;
